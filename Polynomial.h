//
//  Polynomial.h
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#ifndef Polynomial_h
#define Polynomial_h

#include <iostream>
#include <cmath>
#include <cassert>
#include <vector>

class Polynomial {
    typedef unsigned int sizeT;
    sizeT CAPACITY = 1000;
    sizeT used;
    double* data;
    

public:
    ///Constructors///
    // Default constructor
    Polynomial();
    
    // Set the x^0 coefficient only
    Polynomial(double a0);
    
    // Copy constructor
    Polynomial( const Polynomial &source);
    
    // Destroys the object
    ~Polynomial();
    
    ///Self mod operator Overloads///
    
    //reference return
    Polynomial& operator = (const Polynomial& source);
    Polynomial& operator = (double a0);
    
    void operator += (const Polynomial& source);
    void operator *= (const Polynomial& source);
    
    void operator -= (const Polynomial& source);
    void operator /= (const Polynomial& source);
    
    
    /// Modification / Setters ///
    
    //adds specified amount the coefficient of the x^k term.
    void add_tocoef(double amount, unsigned int k);
    void sub_tocoef(double amount, unsigned int k);
    
    //set the x^k term to the new coefficient
    void assing_coef(double new_coefficient, unsigned int k);
    
    //sets all coefficients to zero
    void clear();
    
    
    ///Getters///
    int size() const{
        return used;
    }
    //solve the equation for a certain x value
    double eval(double x) const;
    double operator( )(double x) const;
    
    //I don't even know how to do this, but I will try...
    
    //get the root
    //1.
    //.2
 
    void find_root(
              double& answer,
              bool& success,
              Polynomial::sizeT& iterations,
              const double& guess,
              const Polynomial::sizeT& maximum_it,
              const double& epsilon) const;
    //get derivitive
    //1. Multiply the coefficient to the power if it's greater than zero
    //2. subtract the exponent by one
    //2. simplyfy the equation from there on
    Polynomial getDerivitive();
    //reverse all the coefficiencts Ex: x^2, 2x, 9 to 9x^2, 2x, 1
    bool reserve(size_t number);
    bool isItem(int index);
    //returns a coefficient of a certain term by degree
    double coefficient(unsigned int k) const;
    //returns the highest degree of the polynomial ***If the highest isn't an x then return 0 ***
    sizeT degree() const;
    //returns the exponent of the next term with a nonzeror coefficient after x^k.
    double next_term(unsigned int k) const;
    double previous_term(unsigned int k) const;
    
    double* getData() const;
    
};

Polynomial operator + (const Polynomial& p1, const Polynomial& p2);

Polynomial operator * (const Polynomial& p1, const Polynomial& p2);

Polynomial operator - (const Polynomial& p1, const Polynomial& p2);

Polynomial operator / (const Polynomial& p1, const Polynomial& p2);

bool operator > (const Polynomial& p1, const Polynomial& p2);
bool operator < (const Polynomial& p1, const Polynomial& p2);
//outputs the whole equation
std::ostream& operator << (std::ostream& os, const Polynomial& poly);





#endif /* Polynomial_h */
