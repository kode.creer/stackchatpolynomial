//
//  Polynomial.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"

// Default constructor : ALL ZEROS
Polynomial::Polynomial(){
    data = new double[CAPACITY];
    used = 1;
    for (sizeT i = 0; i < used; ++i) {
        data[i] = 0;
    }
    
    //data[0] = 0;

}

// Set the x^0 coefficient only
Polynomial::Polynomial(double a0){
    data = new double[CAPACITY];
    data[0] = a0;
    used = 0;
    ++used;
    //data[0] = a0;

}

// Copy constructor
Polynomial::Polynomial( const Polynomial &source){
    data = source.data;
    used = source.used;
    CAPACITY = source.CAPACITY;
}
// Copy 
// Destroys the object
Polynomial::~Polynomial(){
    std::cout << "Destroyed" << std::endl;

    delete[]  data;

}



