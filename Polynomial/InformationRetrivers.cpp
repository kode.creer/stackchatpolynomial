//
//  InformationRetrivers.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"
#include <algorithm>

//returns a coefficient of a certain term by degree
double Polynomial::coefficient(unsigned int k) const{
    return data[k];
}

//returns the highest degree of the polynomial ***If the highest isn't an x then return 0 ***
Polynomial::sizeT Polynomial::degree() const{
    
    int i = size()-1;
    while ( (data[i] < 0.00001) && (data[i] > -0.00001) && i != -1) {
        --i;
    }
    return i = i < 0 ? 0 : i;
}

//returns the exponent of the next term with a nonzeror coefficient after x^k.
double Polynomial::next_term(unsigned int k) const{
    
    int iterator = k + 1;
    while (data[iterator] == 0 && iterator < size()) {
        ++iterator;
    }
    return iterator = data[iterator] != 0 ? iterator : UINT_MAX;
}
double Polynomial::previous_term(unsigned int k) const{
    int iterator = k-1;
    
    while ( !(data[iterator] < 0.00001) && !(data[iterator] > -0.00001) && iterator != -1) {
        --iterator;
    }
    return iterator = iterator < 0 ? UINT_MAX: iterator;
}
//get derivitive
//for each value greater than index 0
//1. Multiply the coefficient to the power if it's greater than zero
//2. subtract the exponent by one
//2. simplyfy the equation from there on
Polynomial Polynomial::getDerivitive(){
    
    Polynomial derivative = Polynomial();
    derivative.used = size() - 1;
    for(size_t i = 1; i < size(); ++i){
        double value = data[i];
        if (i > 0) {
            value *= i;
        }
        derivative.data[i-1] += value;
    }
    return derivative;
}
double* Polynomial::getData() const{
    return data;
}
bool Polynomial::isItem(int index){
    return index < used && index >= 0;
}
double Polynomial::eval(double x) const{
    //copy paste and modify !!!!

    double total = 0;

    
    for (int i = 0; i < size(); ++i) {
        double comp1;
        comp1 = data[i];
        total += ( comp1 * (pow(x, i)) );

    }
    return total;
}

//return the last value after the division
double polyDiv(double& multiplyer, const int length, double* data){
    //multiply the current I by the multiplyer
    //add the value to the result of the multiplyer

    double superValue = data[length-1];
    
    for (int i = length-1; i > 0; --i) {
        superValue *= multiplyer;

        superValue += data[i-1];

        //superValue = multiplyer * data[i];

    }
//    std::cout << superValue << " + " << data[0] << std::endl;
//    superValue += data[0];

    return superValue;
}

void Polynomial::find_root(
                 double& answer,
                 bool& success,
                 Polynomial::sizeT& iterations,
                 const double& guess,
                 const Polynomial::sizeT& maximum_it,
                 const double& epsilon) const
{
    //PostCondition. We will just return the first root we find for now
    
    iterations = 0;
    //find the rational zeros
    //I know I am cheating here, sorry....
    std::vector<double> P;
    std::vector<double> Q;
    std::vector<double> PQList;
    std::vector<double> roots;
    
    int length = size();
    
    //get factors of P and Q
    for (int i = 0; i < length; ++i) {
        //data[i] is zero and would cause a crash.
        if (data[i] < epsilon && data[i] > -epsilon) {
            continue;
        }
        if (int(data[size()-1]) % int(data[i]) == 0) {
            Q.push_back(data[i]);
        }
        if (int(data[0]) % int(data[i]) == 0) {
            P.push_back(data[i]);
        }
    }
    if (Q.size() < 2 && Q[0] == 1) {
        Q.push_back(1);
    }
    //get the P/Q list
    for (int i = 0; i < P.size(); ++i) {
        for(int b = 0; b < Q.size(); ++b){
            PQList.push_back(double(P[i] / Q[i]));
        }
    }
    
    std::sort(PQList.begin(), PQList.end());
    //see if it works like a fato
    for (int i = 0; i < maximum_it; ++i) {
        iterations = roots.size() > 1 ? iterations : iterations + 1;
        //try the positive version first
        //then the negative version
        double multiplyer = i < PQList.size() ? PQList[i] : 0;
        double value = polyDiv(multiplyer, length, data);
        if ( (value < epsilon && value > -epsilon) ) {
            answer = multiplyer;
            success = true;
            return;
        }
        multiplyer *= -1;
        value = polyDiv(multiplyer, length, data);
        if ( (value < epsilon && value > -epsilon) ) {
            answer = multiplyer;
            success = true;
            return;
        }
        
    }
    
    success = false;
   
}

