//
//  PolynomialModification.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"

//adds specified amount the coefficient of the x^k term.
void Polynomial::add_tocoef(double amount, unsigned int k){
    data[k] += amount;
}
//subtracts specified amount the coefficient of the x^k term.
void Polynomial::sub_tocoef(double amount, unsigned int k){
    data[k] -= amount;
}
//set the x^k term to the new coefficient
void Polynomial::assing_coef(double new_coefficient, unsigned int k){

    if (k >= CAPACITY) {

        double* copy =  new double[CAPACITY + (k-CAPACITY)];

        for (sizeT i = 0; i < size(); ++i) {

            copy[i] = data[i];
        }
        delete [] data;
        data = new double[CAPACITY];
        for (sizeT i = 0; i < size(); ++i) {
            data[i] = copy[i];
        }
        used += ((k-CAPACITY));
//        for (int i = size()-k; i < size(); ++i) {
//            std::string dat = std::to_string(data[i]);
//            if ( dat.find("e") != std::string::npos) {
//                data[i] = 0;
//            }
//        }
        CAPACITY = used;
        delete [] copy;
    }
    used = !isItem(k) ? ++used : used;
    data[k] = new_coefficient;
    used = data[k] < 0.00001 && data[k] > -0.00001 ? --used : used;
    
}

//sets all coefficients to zero
void Polynomial::clear(){
    for(sizeT i = 0; i < size(); ++i){
        data[i] = 0;
    }
    used = 0;
}

//reverse all the coefficiencts Ex: x^2, 2x, 9 to 9x^2, 2x, 1
bool Polynomial::reserve(size_t number){
    
    //incase someone deciedes to be stupid or don't wnat to enter anything
    return isItem(int(number));
    
}



