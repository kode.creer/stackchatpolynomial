//
//  OperatorExternalMod.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/16/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//


//By extrenal mod I mean stuff that modifies stuff from the outside Ex: + - * /

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"

//Polynomial operator / (const Polynomial& p1, const Polynomial& p2){
//    Polynomial poly = p1 > p2 ? Polynomial() + p1 : Polynomial() + p2;
//    Polynomial oppositePoly = p1 < p2 ? Polynomial() + p2 : Polynomial() + p1;
//    poly /= oppositePoly;
//    return poly;
//}
Polynomial operator - (const Polynomial& p1, const Polynomial& p2) {
    Polynomial dePoly = Polynomial();
    dePoly += p1;
    dePoly -= p2;
    return dePoly;
}
Polynomial operator * (const Polynomial& p1, const Polynomial& p2) {
    Polynomial muPoly = Polynomial();
    muPoly += p1;
    muPoly *= p2;
    return muPoly;
}
Polynomial operator + (const Polynomial& p1, const Polynomial& p2) {
    Polynomial poly = Polynomial();
    poly += p1;
    poly += p2;
    return poly;
}
