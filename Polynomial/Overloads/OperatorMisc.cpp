//
//  OperatorMisc.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/16/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"
#include <cmath>
bool operator > (const Polynomial& p1, const Polynomial& p2){
    int size = p1.size() > p2.size() ? p1.size() : p2.size();
    double* poly1 = p1.getData();
    double* poly2 = p2.getData();
    double total1 = 0;
    double total2 = 0;
    
    for (int i = 0; i < size; ++i) {
        double comp1;
        double comp2;
        if (i >= p2.size()) {
            comp2 = 0;
            comp1 = poly1[i];
        }
        else if (i >= p1.size()){
            comp1 = 0;
            comp2 = poly2[i];
        }
        else {
            comp1 = poly1[i];
            comp2 = poly2[i];
        }
        
        total1 += (comp1 * (pow(2, i)) );
        total2 += (comp2 * (pow(2, i)) );
    }

    return total1 > total2;
}
bool operator < (const Polynomial& p1, const Polynomial& p2){
    int size = p1.size() > p2.size() ? p1.size() : p2.size();
    double* poly1 = p1.getData();
    double* poly2 = p2.getData();
    double total1 = 0;
    double total2 = 0;
    
    for (int i = 0; i < size; ++i) {
        double comp1;
        double comp2;
        if (i >= p2.size()) {
            comp2 = 0;
            comp1 = poly1[i];
        }
        else if (i >= p1.size()){
            comp1 = 0;
            comp2 = poly2[i];
        }
        else {
            comp1 = poly1[i];
            comp2 = poly2[i];
        }
        
        total1 += (comp1 * (pow(2, i)) );
        total2 += (comp2 * (pow(2, i)) );
    }

    return total1 < total2;
}
std::ostream& operator << (std::ostream& os, const Polynomial& poly){
    
    double* data = poly.getData();
    
    for (int i = poly.size()-1; i > 0; --i) {
        
        if (data[i] != 0 && i != 0){
            os << data[i] << "x^" << i << " + ";
            continue;
        }
        else if( data[i] < 0.000001 && data[i] > -0.000001){
            continue;
        }
        
    }
    os << data[0];
    return os;
}

double Polynomial::operator( )(double x) const{
    return eval(x);
}
