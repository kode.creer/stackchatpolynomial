//
//  main.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "Polynomial.h"
// FILE: polytest.cxx
// An Interactive test program for the polynomial ADT
// Written by: Kenneth R. Glover - gloverk@colorado.edu
#include "ConsoleTimeSaver/ConsoleTimeSaver.h"
#include <cctype>          // Provides toupper
#include <iostream>        // Provides cout and cin
#include <cstdlib>         // Provides EXIT_SUCCESS
using namespace std;
//using namespace main_savitch_4;

const unsigned int MANY = 3; // Number of polynomials allowed in this test program.


// PROTOTYPES for functions used by this test program:
void print_menu();
// Postcondition: The menu has been written to cout.

size_t set_current( );
// Postcondition: Return value is index for a new current polynomial.

char get_command();
// Postcondition: The user has been prompted for a character.
// The entered charatcer will be returned, translated to upper case.

void view(const Polynomial& test);
//Postcondition:  The polynomial passed has been sent to cout.

void view_all(const Polynomial a[]);
//Postcondition:  All polynomials has been written to cout.


void test_assign(Polynomial& test);
// Postcondition: The user has been prompted for the degree and the coeffinient
// to be set.  The resulting polynomial has been written to cout.




int main()
{
    Polynomial p[MANY];
    size_t current_index = 0;
    char command;
    size_t i;
    
    cout << "Polynomials ";
    testArrs(p, MANY);
    for (i = 0; i < MANY; ++i)
        cout << char('A' + i) << ' ';
    cout << "have all been initialized." << endl;
    
    do
    {
        print_menu();
        command = toupper(get_command());
        
        switch(command)
        {
            case 'S':  current_index = set_current( );
                break;
            case '1':  test_assign(p[current_index]);
                break;
            case '2':  test_add(p[current_index]);
                break;
            case 'C':  test_clear(p[current_index]);
                break;
            case 'V':
                cout << char(current_index + 'A') << ": ";
                view(p[current_index]);
                break;
            case 'A':  view_all(p);
                break;
            case '+':
                cout << "A + B: ";
                view(p[0] + p[1]);
                break;
            case '-':
                cout << "A - B: ";
                view(p[0] - p[1]);
                break;
            case '*':
                cout << "A * B: ";
                view(p[0] * p[1]);
                break;
            case 'Q':  // Do nothing..
                break;
            default:   cout << "Invalid command." << endl;
                break;
        }
        
    }
    while(command != 'Q');

    return (EXIT_SUCCESS);
}

void print_menu()
{
    cout << "----------------- The Commands -----------------" << endl;
    cout << "S - set the current Polynomial to work on" << endl;
    cout << "V - view the current polynomial by using <<" << endl;
    cout << "A - view all polynomials by using <<" << endl;
    cout << "+ - view A + B" << endl;
    cout << "- - view A - B" << endl;
    cout << "* - view A * B" << endl;
    cout << "  -   -   -   -   -   -   -   -   -   -   -   -" << endl;
    cout << "Q - quit this interactive test program" << endl;
    cout << "-------------------------------------------------" << endl;
}

char get_command()
{
    char command;
    
    cout << ">";
    cin >> command;
    
    return(toupper(command));
}

void view(const Polynomial& test)
{
    cout << test
    << " (degree is " << test.degree( ) << ")" << endl;
}

size_t set_current( )
{
    size_t i;
    char command;
    
    do
    {
        cout << "Polynomials ";
        for (i = 0; i < MANY; ++i)
            cout << char('A' + i) << ' ';
        cout << "." << endl;
        cout << "Enter the polynomial you want to work on: ";
        command = toupper(get_command());
    }
    while ((command < 'A') || (command >= char('A' + MANY)));
    return command - 'A';
}



void view_all(const Polynomial p[])
{
    size_t i;
    
    cout << endl;
    
    for (i = 0; i < MANY; ++i)
    {
        cout << char(i + 'A') << ": ";
        view(p[i]);
    }
}

